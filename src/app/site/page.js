import HomePage from "../../components/Homepage";

const page = () => {
  return (
    <div>
      <HomePage />
    </div>
  );
};

export default page;

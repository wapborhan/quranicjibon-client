import Homepage from "@/components/Homepage";
// import MainTain from "./maintain";
import "../assets/stylesheet/homepage/color.css";
// import "../assets/stylesheet/homepage/perfect-scrollbar.css";
import "../assets/stylesheet/homepage/responsive.css";
import "../assets/stylesheet/homepage/slick.css";
import "../assets/stylesheet/custom.css";

import "../assets/stylesheet/homepage/style.css";

export default function Home() {
  return (
    <>
      {/* <MainTain /> */}
      <Homepage />
    </>
  );
}
